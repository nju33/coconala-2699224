import camelcaseKeys from 'camelcase-keys';

export const init = widget => {
  return {
    type: 'init',
    payload: camelcaseKeys(widget),
  }
}
