import camelcaseKeys from 'camelcase-keys';

export const patchWidget = obj => {
  return {
    type: 'patchWidget',
    payload: camelcaseKeys(obj),
  }
}
