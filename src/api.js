import path from 'path';

export default {
  '/widget/:widgetUrl'(widgetUrl) {
    return ENDPOINT + path.join('widget', widgetUrl);
  },
  get '/feedback/post'() {
    return ENDPOINT + 'feedback/post';
  },
  get '/feedback/put'() {
    return ENDPOINT + 'feedback/put';
  },
}
