import styled from 'styled-components';
import {ButtonBase} from './button';

export const ButtonGroup = styled.span`
  display: flex;

  & > ${ButtonBase} {
    flex: auto;
    margin: 0 1em;
  }

  & > ${ButtonBase}:first-child {
    margin-left: 0;
  }

  & > ${ButtonBase}:last-child {
    margin-right: 0;
  }
`;
