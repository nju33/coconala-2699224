import React from 'react';
import styled from 'styled-components';

const getColor = props => props.theme.color;

export const ButtonBase = styled.button.attrs({
  type: 'button'
})`
  user-select: none;
  padding: .375rem .75rem;
  border-radius: .25rem;
  font-size: 1rem;
  transition: .15s ease-in-out;
  outline: none;
  cursor: pointer;

  background: transparent;
  ${props => {
    const color = getColor(props);

    return `
      color: ${color};
      border: 1px solid ${color};
    `;
  }};

  &:hover,
  &:active {
    ${props => {
      const color = getColor(props);

      return `
        background: ${color};
        color: #fff;
      `;
    }}
  };

  &:focus {
    bos-shadow: 0 0 0px 2px #c5deff;
  }
`;

const createButton = color => {
  return props => <ButtonBase {...props} theme={{color}} />;
};

export const FlashyButton = createButton('#17a2b8');
export const Button = createButton('#6c757d');
