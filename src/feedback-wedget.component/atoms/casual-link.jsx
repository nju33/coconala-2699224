import styled from 'styled-components';

export const CasualLink = styled.a`
  color: #212529;
  background: #f8f9fa;
  padding: .15em .75em;
  border-radius: 160px;
  transition: .1s;

  &:hover,
  &:active {
    background: #dae0e5;
  }
`;
