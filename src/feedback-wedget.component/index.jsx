import React from 'react';
import PropTypes from 'prop-types';
import {injectGlobal} from 'styled-components';
import {withRouter, Switch, Route, Redirect} from 'react-router-dom';
import axios from 'axios';
import camelcaseKeys from 'camelcase-keys';
import api from '../api';
import {connect} from 'react-redux';
import {init} from '../actions';
import {
  CasualLink,
  Center,
  Loading,
  Message,
  Right,
  FlashyButton,
  ButtonGroup,
  Cenkkter,
  Wrapper
} from './atoms';
import {BadPage, DonePage, TopPage} from './pages';

class FeedbackWedgetBase extends React.Component {
  static propTypes = {
    widgetUrl: PropTypes.string.isRequired
  };

  render() {
    let loading = null;
    if (typeof this.props.widget === 'undefined') {
      loading = (
        <Center>
          <Loading />
        </Center>
      );
    }

    return (
      <>
        {loading}
        <Switch>
          <Route exact path="/done" component={DonePage} />
          <Route exact path="/bad" component={BadPage} />
          <Route exact path="/" component={TopPage} />
          <Redirect to="/" />
        </Switch>

        <Wrapper style={{ marginTop: '1em'}}>
          <Right style={{fontSize: '12px', fontWeight: 'bold'}}>
            <CasualLink href="//lp.powerfeedback.io/">by PowerFeedback</CasualLink>
          </Right>
        </Wrapper>
      </>
    );
  }

  async componentDidMount() {
    injectGlobal`
      body {
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
        font-size: 16px;
      }

      a {
        color: inherit;
        text-decoration: none;
      }
    `;

    const res = await axios.get(
      api['/widget/:widgetUrl'](this.props.widgetUrl)
    );
    const data = camelcaseKeys(res.data);
    if (data.apiStatus === 'error') {
      return alert(data.message);
    }

    this.props.init(res.data);
  }
}

export const FeedbackWedget = withRouter(
  connect(
    state => state,
    (dispatch, props) => {
      return {
        init(data) {
          data.url = props.widgetUrl;
          dispatch(init(data));
        }
      };
    }
  )(FeedbackWedgetBase)
);
