import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import {connect} from 'react-redux';
import api from '../../api';
import {init} from '../../actions';
import {Message, FlashyButton, ButtonGroup, Center} from '../atoms';

class ConfirmBase extends React.Component {
  static propTypes = {
    onGoodClick: PropTypes.func.isRequired,
    onBadClick: PropTypes.func.isRequired,
  }

  render() {
    if (!{}.hasOwnProperty.call(this.props, 'widget')) {
      return null;
    }

    return (
      <>
        <Center style={{marginBottom: '1.5em'}}>
          <Message>{this.props.widget.messageGiveMeComment}</Message>
        </Center>
        <ButtonGroup>
          <FlashyButton onClick={this.props.onGoodClick}>{this.props.widget.btnGood}</FlashyButton>
          <FlashyButton onClick={this.props.onBadClick}>{this.props.widget.btnBad}</FlashyButton>
        </ButtonGroup>
      </>
    );
  }

  // async componentDidMount() {
  //   const res = await axios.get(
  //     api['/widget/:widgetUrl'](this.props.widgetUrl)
  //   );
  //   this.props.init(res.data);
  // }
}

export const Confirm = connect(
  state => state,
  // dispatch => {
  //   return {
  //     init(data) {
  //       dispatch(init(data));
  //     }
  //   };
  // }
)(ConfirmBase);
