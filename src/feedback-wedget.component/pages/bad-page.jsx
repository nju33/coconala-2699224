import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import axios from 'axios';
import camelcaseKeys from 'camelcase-keys';
import qs from 'query-string';
import api from '../../api';
import {Wrapper, Center, Textarea, Button} from '../atoms';
import {Confirm} from '../organisms';

class BadPageBase extends React.Component {
  refTextarea = textarea => {
    this.textarea = textarea;
  };

  onSubmit = e => {
    e.preventDefault();

    if (this.textarea === null || this.textarea.value === '') {
      return;
    }

    if (
      typeof this.props.widget === 'undefined' ||
      typeof this.props.widget.feedbackId === 'undefined'
    ) {
      return alert('feedback_idがありません');
    }

    const query = qs.stringify({
      widget_url: this.props.widget.url,
      feedback_id: this.props.widget.feedbackId,
      feedback_comment: this.textarea.value
    });

    const url = `${api['/feedback/put']}?${query}`;

    axios
      .put(url)
      .then(res => {
        const data = camelcaseKeys(res.data);
        if (data.apiStatus === 'success') {
          return this.props.history.push('/done');
        }

        alert(data.message);
      })
      .catch(err => {
        alert(err.message);
      });
  };

  render() {
    if (this.props.widget === undefined) {
      return null;
    }

    return (
      <Wrapper>
        <form onSubmit={this.onSubmit}>
          <Textarea
            innerRef={this.refTextarea}
            placeholder="サービス改善の為ご意見を頂けますか？"
          />
          <Button type="submit" style={{width: '100%'}}>
            Send feedback
          </Button>
        </form>
      </Wrapper>
    );
  }

  componentDidMount() {
    if (
      typeof this.props.widget === 'undefined' ||
      typeof this.props.widget.feedbackId === 'undefined'
    ) {
      this.props.history.push('/');
    }
  }
}

export const BadPage = withRouter(connect(state => state)(BadPageBase));
