import React from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {Wrapper, Center} from '../atoms';
import {Confirm} from '../organisms';

class DonePageBase extends React.Component {
  render() {
    if (typeof this.props.widget === 'undefined') {
      return null;
    }

    return (
      <Wrapper>
        <Center>{this.props.widget.messageThankyou}</Center>
      </Wrapper>
    )
  }

  componentDidMount() {
    if (typeof this.props.widget === 'undefined') {
      this.props.history.push('/');
    }
  }
}

export const DonePage = withRouter(
  connect(
    state => state,
  )(DonePageBase)
);
