import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import qs from 'query-string';
import camelcaseKeys from 'camelcase-keys';
import api from '../../api';
import {patchWidget} from '../../actions';
import {Wrapper} from '../atoms';
import {Confirm} from '../organisms';

class TopPageBase extends React.Component {
  getData(btnNumber) {
    return {
      btn: btnNumber,
      widget_url: this.props.widget.url,
      http_referer: window.location.href,
      http_user_agent: window.navigator.userAgent
    };
  }

  handleGoodClick = () => {
    const url = `${api['/feedback/post']}?${qs.stringify(this.getData(1))}`;
    axios
      .post(url)
      .then(res => {
        const data = camelcaseKeys(res.data);
        if (data.apiStatus === 'success') {
          return this.props.history.push('/done');
        }

        alert(data.message);
      })
      .catch(err => {
        alert(err.message);
      });
  };

  handleBadClick = () => {
    const url = `${api['/feedback/post']}?${qs.stringify(this.getData(2))}`;

    axios
      .post(url)
      .then(res => {
        const data = camelcaseKeys(res.data);
        if (data.apiStatus === 'success') {
          this.props.setFeedbackId({feedbackId: data.feedbackId});
          return this.props.history.push('/bad');
        }

        alert(data.message);
      })
      .catch(err => {
        alert(err.message);
      });
  };

  render() {
    return (
      <Wrapper>
        <Confirm
          onGoodClick={this.handleGoodClick}
          onBadClick={this.handleBadClick}
        />
      </Wrapper>
    );
  }
}

export const TopPage = withRouter(
  connect(
    state => state,
    dispatch => {
      return {
        setFeedbackId(obj) {
          dispatch(patchWidget(obj));
        }
      };
    }
  )(TopPageBase)
);
