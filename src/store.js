import {createStore} from 'redux';

const reducer = (state = {}, action) => {
  switch (action.type) {
    case 'init': {
      return {
        ...state,
        widget: action.payload
      };
    }
    case 'patchWidget': {
      return {
        ...state,
        widget: {...state.widget, ...action.payload}
      };
    }
    default: {
      return state;
    }
  }
};

export default createStore(reducer);
